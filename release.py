import os
import sys
import argparse
import subprocess
import git


# parse/check existing package info
proc = subprocess.run(
    [sys.executable, 'setup.py', '--name', '--version'],
    capture_output=True,
    text=True,
)
try:
    proc.check_returncode()
except subprocess.CalledProcessError as e:
    sys.exit("Failed to execute setup.py:\n\n{}".format(proc.stderr.strip()))
PACKAGE, CVERS = proc.stdout.strip().split('\n')
VFILE = os.path.join('inspiral_range', '_version.py')
if os.path.exists(VFILE):
    with open(VFILE) as f:
        exec(f.read())
    if CVERS != __version__:
        sys.exit("""Package versions do not match:
{:>{w}}: {}
{:>{w}}: {}""".format('setup.py', __version__, VFILE, CVERS, w=len(VFILE)))


def vers_from_str(s):
    """Return version info from version string

    Version is assumed to be "semantic" [https://semver.org/] of the
    form:

       major.minor.rev+build

    where major, minor, rev will all be ints, and build will be a str.
    Returns tuple of all components.

    """
    # check if there's a build suffix on the string
    build = ''
    vsplit = s.split('+')
    if len(vsplit) > 1:
        build = '+'+vsplit[1]
    try:
        major, minor, rev = vsplit[0].split('.')
    except ValueError:
        raise ValueError("Could not parse version string: {}".format(s))
    return int(major), int(minor), int(rev), build


def write_vfile(vers):
    print("writing version '{}' to {}...".format(vers, VFILE))
    with open(VFILE, 'w') as f:
        f.write("__version__ = '{}'\n".format(vers))


parser = argparse.ArgumentParser(
    description="""Make new release of this python package:

   package name: {}
   version file: {}
current version: {}

The new version will be based on current version and release type
(major.minor.rev+build).  The following will then be done:

* version file will be updated and committed to the git repo
* repo will be tagged with new version
* sdist and wheel packages will be generated
* a second commit will update the repo to dev version ('+dev0')
""".format(PACKAGE, VFILE, CVERS),
    formatter_class=argparse.RawDescriptionHelpFormatter,
)
cgroup = parser.add_mutually_exclusive_group(required=True)
cgroup.add_argument(
    'release',
    choices=['major', 'minor', 'rev'],
    nargs='?',
    help="release type",
)
cgroup.add_argument(
    '-s', '--set',
    metavar='VERSION',
    type=str,
    help="set version (no git commits or package builds)",
)


def main():
    args = parser.parse_args()

    if args.set:
        vers_from_str(args.set)
        write_vfile(args.set)
        exit()

    release = args.release

    major, minor, rev, build = vers_from_str(CVERS)

    if release == 'major':
        major += 1
        minor = 0
        rev = 0
    elif release == 'minor':
        minor += 1
        rev = 0
    elif release == 'rev':
        rev += 1

    version = '{}.{}.{}'.format(major, minor, rev)

    try:
        resp = input("new {} release: {} -> {}\ntype 'yes' to confirm: ".format(release, CVERS, version))
        if resp != 'yes':
            exit("abort.")
    except KeyboardInterrupt:
        exit("\nabort.")

    tags = subprocess.run(['git', 'tag'], capture_output=True, text=True).stdout.split()
    if version in tags:
        exit("git tag already exists for '{}'; aborting.".format(version))

    write_vfile(version)

    msg = "new {} release {}".format(release, version)

    repo = git.Repo()
    print("git commit...")
    repo.git.commit('-m', msg, VFILE)
    print("git tag '{}'...".format(version))
    repo.git.tag('-m', msg, version)

    print("generating sdist and wheel...")
    out = subprocess.run(
        [sys.executable, 'setup.py', 'sdist', 'bdist_wheel'],
        check=True,
        capture_output=True,
        text=True,
    )
    print(out.stdout.strip())

    print("update to dev version...")
    dvers = version + '+dev0'
    write_vfile(dvers)
    repo.git.commit('-m', "post release dev versioning", VFILE)


if __name__ == '__main__':
    main()
